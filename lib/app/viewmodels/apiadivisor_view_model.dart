import 'package:architecture/app/models/api_adivisor_model.dart';
import 'package:architecture/app/repositories/apiadivisor_repository_interface.dart';
import 'package:flutter/material.dart';

class ApiAdivisorViewModel {
  final IApiAdvisor repository;

  final apiAdivisorModel = ValueNotifier<ApiadvisorModel>(null);

  ApiAdivisorViewModel(this.repository);

  fill() async {
    try {
      apiAdivisorModel.value = await repository.getTime();
    } catch (e) {}
  }
}
