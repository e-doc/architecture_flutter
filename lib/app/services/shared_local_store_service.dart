import 'package:architecture/app/interfaces/local_storage_interface.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SharedLocalStoreService implements LocalStorageInterface {
  @override
  Future delete(String key) async {
    var instance = await SharedPreferences.getInstance();
    return instance.remove(key);
  }

  @override
  Future get(String key) async {
    var instance = await SharedPreferences.getInstance();
    return instance.get(key);
  }

  @override
  Future put(String key, value) async {
    var instance = await SharedPreferences.getInstance();

    if (value is bool) {
      instance.setBool(key, value);
    } else if (value is double) {
      instance.setDouble(key, value);
    } else if (value is int) {
      instance.setInt(key, value);
    } else if (value is String) {
      instance.setString(key, value);
    } else if (value is List<String>) {
      instance.setStringList(key, value);
    }

    return null;
  }
}
