import 'package:architecture/app/models/api_adivisor_model.dart';
import 'package:architecture/app/viewmodels/apiadivisor_view_model.dart';
import 'package:flutter/material.dart';

class HomeController {
  final ApiAdivisorViewModel viewModel;

  HomeController(this.viewModel);

  ValueNotifier<ApiadvisorModel> get time => viewModel.apiAdivisorModel;

  getTime() {
    viewModel.fill();
  }
}
