import 'package:architecture/app/models/api_adivisor_model.dart';
import 'package:architecture/app/pages/home/home_controller.dart';
import 'package:architecture/app/repositories/apiadivisor_repository.dart';
import 'package:architecture/app/services/client_http_service.dart';
import 'package:architecture/app/viewmodels/apiadivisor_view_model.dart';
import 'package:flutter/material.dart';
import 'components/custom_switch_widget.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final HomeController controller = HomeController(
      ApiAdivisorViewModel(ApiadvisorRepository(ClientHttpService())));

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Home Page'),
      ),
      floatingActionButton: FloatingActionButton(
          child: Icon(Icons.account_balance),
          onPressed: () {
            controller.getTime();
          }),
      body: Center(
        child: Column(
          children: <Widget>[
            CustomSwitchWidget(),
            ValueListenableBuilder<ApiadvisorModel>(
              valueListenable: controller.time,
              builder: (context, model, child) {
                if (model?.text == null) {
                  return Center(
                    child: CircularProgressIndicator(),
                  );
                }

                return Text(model.text);
              },
            )
          ],
        ),
      ),
    );
  }
}
