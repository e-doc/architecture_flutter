import 'package:architecture/app/models/api_adivisor_model.dart';

abstract class IApiAdvisor {
  Future<ApiadvisorModel> getTime();
}
