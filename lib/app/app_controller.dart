import 'package:architecture/app/interfaces/local_storage_interface.dart';
import 'package:architecture/app/models/app_config_model.dart';
import 'package:architecture/app/services/shared_local_store_service.dart';
import 'package:architecture/app/viewmodels/change_theme_view_model.dart';
import 'package:flutter/cupertino.dart';

class AppController {
  final ChangeThemeViewModel changeThemeViewModel;

  AppController(this.changeThemeViewModel) {
    changeThemeViewModel.init();
  }

  bool get isDark => changeThemeViewModel.config.themeSwitch.value;

  ValueNotifier<bool> get themeSwitch =>
      changeThemeViewModel.config.themeSwitch;
}
