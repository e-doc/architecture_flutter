import 'package:architecture/app/repositories/apiadivisor_repository_interface.dart';
import 'package:architecture/app/services/client_http_service.dart';
import 'package:architecture/app/services/shared_local_store_service.dart';
import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';

import 'app_controller.dart';
import 'app_widget.dart';
import 'interfaces/client_http_interface.dart';
import 'interfaces/local_storage_interface.dart';
import 'pages/home/home_controller.dart';
import 'pages/home/home_page.dart';
import 'pages/login/login_page.dart';
import 'repositories/apiadivisor_repository.dart';
import 'viewmodels/apiadivisor_view_model.dart';
import 'viewmodels/change_theme_view_model.dart';

class AppModule extends MainModule {
  @override
  List<Bind> get binds {
    return [
      Bind((i) => HomeController(i.get())),
      Bind((i) => ApiAdivisorViewModel(i.get())),
      Bind<IApiAdvisor>((i) => ApiadvisorRepository(i.get())),
      Bind<IClientHttp>((i) => ClientHttpService()),
      Bind((i) => AppController(i.get()), lazy: false),
      Bind((i) => ChangeThemeViewModel(storage: i.get())),
      Bind<LocalStorageInterface>((i) => SharedLocalStoreService()),
    ];
  }

  @override
  Widget get bootstrap => AppWidget();

  @override
  List<Router> get routers => [
        Router('/', child: (context, args) => LoginPage()),
        Router('/home', child: (context, args) => HomePage())
      ];
}
