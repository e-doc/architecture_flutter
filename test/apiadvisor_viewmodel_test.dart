import 'package:architecture/app/interfaces/client_http_interface.dart';
import 'package:architecture/app/models/api_adivisor_model.dart';
import 'package:architecture/app/repositories/apiadivisor_repository.dart';
import 'package:architecture/app/viewmodels/apiadivisor_view_model.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';

class ClientHttpMock implements IClientHttp {
  @override
  void addToken(String token) {}

  @override
  Future get(String url) async {
    return [
      ApiadvisorModel(country: "BR", date: "2020/05/30", text: "fdsff")
          .toJson(),
    ];
  }
}

class ClientHttpErrorMock implements IClientHttp {
  @override
  void addToken(String token) {}

  @override
  Future get(String url) async {
    return [];
  }
}

class ClientHttpMockito extends Mock implements IClientHttp {}

main() {
  final mock = ClientHttpMockito();

  final viewModel = ApiAdivisorViewModel(
    ApiadvisorRepository(
      mock,
    ),
  );

  group("ApiAdvisorViewModel", () {
    test('ApiAdvisorViewModel error', () async {
      when(mock.get(
              "http://apiadvisor.climatempo.com.br/api/v1/anl/synoptic/locale/BR?token=95a9cb1ae6b422e8d7b7a4fe634fb14f"))
          .thenThrow(Exception("error"));

      await viewModel.fill();
      expect(viewModel.apiAdivisorModel.value, null);
    });

    test('ApiAdvisorViewModel success', () async {
      when(mock.get(
              "http://apiadvisor.climatempo.com.br/api/v1/anl/synoptic/locale/BR?token=95a9cb1ae6b422e8d7b7a4fe634fb14f"))
          .thenAnswer((_) => Future.value([
                ApiadvisorModel(
                        country: "BR", date: "2020/05/30", text: "fdsff")
                    .toJson(),
              ]));
      await viewModel.fill();
      expect(viewModel.apiAdivisorModel.value, isA<ApiadvisorModel>());
    });
  });
}
